\documentclass[12pt,a4paper]{article}

\input{include/preamble}

%------------------------------------------------------------------------------
\usepackage{tikz}

\newcommand{\Arg}{\textsf{arg}}
\newcommand{\dinf}{\textsf{DInf}}
\newcommand{\ufpr}{\textsf{UFPR}}
\newcommand{\cnpq}{\Link{\textsf{CNPq}}{http://www.cnpq.br}}
\newcommand{\todo}{\link{todo}{sec:todo}}


%==============================================================================
\title{Coloração de grafos é NP-Difícil}
\author{Paulo Guilherme Inça}
 
\hypersetup{pdftitle={\thetitle},pdfauthor={\theauthor}} 

%==============================================================================
\begin{document}
\flushbottom
\maketitle
\tableofcontents

%..............................................................................
\section{Introdução}

As primeiras aparições na literatura de problemas de coloração foram
relacionadas à coloração de mapas. Nessa época foi proposta a
conjectura das quatro cores, que dizia ser possível colorir qualquer
mapa com apenas quatro cores de forma que nenhum vizinho tivesse a
mesma cor. A primeira solução foi apresentada em 1879 por Alfred Kempe,
que, no entanto, se mostrou estar errada alguns anos mais tarde.
O problema ficou em aberto até o ano de 1976, quando
se tornou o teorema da quatro cores, após prova, dessa vez definitiva,
apresentada em 1976 por Kenneth Appel e Wolfgang Haken \cite{Connor1996}.

A coloração de mapas está intrinsecamente relacionada a grafos planares.
No entanto, coloração de grafos em geral vem sendo estudado
computacionalmente desde o início dos anos 70. O problema de encontrar
o número cromático de um grafo foi apresentado como um dos 21 
problemas NP-Completo de Karp em 1972 \cite{Karp72}. 

Esse trabalho está estruturado da seguinte forma: na Seção 2 veremos uma
introdução à notação usada e ao problema da coloração de grafos,
tanto na sua versão de otimização quanto de decisão. Na Seção 3,
provaremos que 3-coloração é NP-Completo. Na Seção 4, falaremos sobre
a generalização de 3-coloração para K-coloração, assim como a
importância de analisar as restrições dos problemas. A Seção 5 conclui
o trabalho.

%..............................................................................
\section{O Problema da Coloração de Grafos}

Um dos interesses no estudo do problema de coloração de grafos é
sua vasta aplicabilidade em diversas áreas. As mais conhecidas são
em problemas de agendamento, o problema da alocação de registradores
para otimização de compiladores e resolução de puzzles.

Antes de vermos a definição do problema precisamos de
algumas definições preliminares. Seja G(V, E) um grafo, onde V é o
conjunto de vértices e E é o conjunto de arestas, e seja
$c: V \leftarrow \{1,...,K\}$ uma função que atribui um dos K números
para cada vértice. O conjunto do contradomínio pode conter qualquer
objeto, no nosso caso são cores. Uma K-coloração de G é dita
apropriada se $c(u) \neq c(v)$ $\forall$ $\{u,v\}$ $\in$ $E$.

Formalmente, o problema de coloração de grafos é apresentado da
seguinte forma:

\begin{problema} Coloração

\noindent Instância: Um grafo G. \\\\
Resposta: O mínimo de cores necessárias para colorir G.

\end{problema}

A versão de decisão do mesmo problema responde se é possível
colorir um grafo com um número especificado de cores. Formalmente:

\begin{problema} K-coloração

\noindent Instância: Um grafo G e um inteiro K. \\\\
Resposta: É possível colorir G com K cores?

\end{problema}

O problema de otimização é polinomialmente redutível
ao problema de decisão, utilizando a versão de
decisão em uma busca binária para encontrar o K mínimo. 

Para provar que coloração de grafos é NP-Difícil basta provar que
a versão de decisão K-coloração é NP-Completo. Para isso vou
começar mostrando que 3-coloração é NP-Completo, sua definição é:

\begin{problema} 3-coloração

\noindent Instância: Um grafo G. \\\\
Resposta: É possível colorir G com 3 cores?

\end{problema}
 
Entendido as definições preliminares, podemos agora ver como
reduzir problemas NP-Completo para 3-coloração. 

%..............................................................................
\section{3-Coloração é NP-Completo}

Para provar que um dado problema X é NP-Completo precisamos
mostrar que existe uma redução em tempo polinomial de um
problema Y para X, denotado por $Y \preceq X$, onde já
sabemos que Y é NP-Completo. Além disso, 
precisamos provar que X pertence a classe NP.

\begin{teorema}
  3-coloração é NP-Completo \cite{Garey74}
\end{teorema}
\begin{proof}
Para mostrar que 3-coloração está em NP basta usar como
certificado uma coloração c. Dado um grafo G(V, E) e um certificado c,
um verificador checa em tempo $\mathcal{O}(n^2)$ se c é uma coloração 
apropriada de G olhando se as pontas de uma aresta tem cores 
diferentes, para toda aresta em E.
Para provar que 3-coloração é NP-difícil vamos mostrar que 3-SAT $\preceq$ 
3-coloração. Ou seja, dado uma instância de 3-SAT, vamos construir um grafo
G, em tempo polinomial, onde G é 3-colorável se e somente se a instância de
3-SAT é satisfatível.

Uma instância de 3-SAT é um par (V, F), onde V é um conjunto de
literais da forma $\{x_1,...,x_n\}$, e F é uma fórmula sobre V
cujas cláusulas tem exatamente 3 literais, e cada cláusula em V 
será denotado por $C_1,C_2,...,C_m$.

G será construído baseado em gadgets, que nada mais é do que uma
construção que simula partes do comportamento de outro problema
computacional \cite{Garey79}. G terá três gadgets, onde o 
primeiro será o conjunto dos vértices $\{T, F, B\}$ conectados 
formando um triângulo. Imaginemos $\{T, F, B\}$ como o conjunto 
de cores a ser usado para colorir G. Como temos um triângulo, 
inicialmente já precisamos de três cores para G.

\begin{center}
\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={circle,draw=black}]
  \node[fill=green] (nT) at (1,10) {T};
  \node[fill=red] (nF) at (3,10)  {F};
  \node[fill=blue] (nB1) at (2,8)  {B};
  
  \foreach \from/\to in {nT/nF,nT/nB1,nF/nB1}
    \draw (\from) -- (\to);

\end{tikzpicture}
\end{center}

O segundo gadget adiciona dois vértices $v_i$ e $\bar{v_i}$,
também formando um triângulo com a base, para cada 
$x_i$ $\in$ $V$. Nesse caso, garantimos que um
se literal receber a cor verdadeira obrigatoriamente sua
negação deverá receber a cor falsa para satisfazer a
coloração de G.

\begin{center}
\begin{tikzpicture}
	[scale=.5,auto=left,every node/.style={circle,draw=black}]
  \node[fill=blue] (nB2) at (8,10)  {B};
  \node[label=center:v1] (nv1) at (7,8)  {\phantom{0}};
  \node[label=center:$\bar{v1}$] (nnv1) at (9,8)  {\phantom{0}};

  \foreach \from/\to in {nv1/nnv1,nv1/nB2,nnv1/nB2}
    \draw (\from) -- (\to);

\end{tikzpicture}
\end{center}

O terceiro gadget também conhecido como OR-gadget conecta
os literais de uma cláusula, simulando uma porta OR, aos vértices
base e falso, para cada $C_1,C_2,...,C_m$ $\in$ $F$. Esse gadget
captura o comportamento da satistatibilidade de cada clausula
da seguinte forma:

\begin{enumerate}
\item Se a, b e c receberem a cor falsa, então o vértice $j_6$
  irá obrigatoriamente receber a cor falsa. Capturando
  a insatisfatibilidade da cláusula $C_i = (a \vee b \vee c)$.
\item Se um dos a, b ou c receber a cor verdadeiro, então existe
  uma coloração válida onde o vértice $j_6$ recebe a cor
  verdadeiro. Capturando assim a satisfatibilidade da cláusula.
\end{enumerate}

\begin{center}
\begin{tikzpicture}
  [scale=.5,auto=left,every node/.style={circle,draw=black}]
  \node[] (na) at (1,5)     {a};
  \node[] (nb) at (1,3)     {b};
  \node[] (nc) at (1,1)     {c};
  \node[label=center:$j_1$] (nj1) at (3,5)    {\phantom{0}};
  \node[label=center:$j_2$] (nj2) at (3,3)    {\phantom{0}};
  \node[label=center:$j_3$] (nj3) at (5,4)    {\phantom{0}};
  \node[label=center:$j_4$] (nj4) at (7,4)    {\phantom{0}};
  \node[label=center:$j_5$] (nj5) at (7,1)    {\phantom{0}};
  \node[label=center:$j_6$] (nj6) at (9,2.5)  {\phantom{0}};
	\node[fill=blue]  (nB) at (12,2.5)  {B};
	\node[fill=green] (nT) at (14,4.5)    {T};
	\node[fill=red]   (nF) at (14,0.5)    {F};
  
  \foreach \from/\to in {na/nj1,nb/nj2,nj1/nj2,nj1/nj3,nj2/nj3,nj3/nj4,nc/nj5,nj4/nj5,nj4/nj6,nj5/nj6, nj6/nB, nj6/nF, nB/nT, nB/nT, nF/nT, nB/nF}
    \draw (\from) -- (\to);

\end{tikzpicture}
\end{center}

É trivial perceber que G é construído em tempo polinomial com
relação ao tamanho da instância (V, F), já que adicionamos
uma quantidade constante de vértices a G para cada literal 
em V e também adicionamos uma quantidade contante de vértices
par cada cláusula em F, além dos três vértices iniciais.

Portanto, após mostrar que 3-coloração é NP-Difícil e que
o problema está contido na classe NP, podemos concluir que
3-coloração é NP-Completo.

\end{proof}

Para ilustrar a construção completa do grafo de coloração usarei
como exemplo a instância $(a \vee b \vee c) \wedge (b \vee \bar{c} \vee \bar{d}) \wedge (\bar{a} \vee c \vee d) \wedge (a \vee \bar{b} \vee \bar{d})$ de 3-SAT
Nesse caso, a = c = Verdadeiro e b = d = Falso, e portanto a
instância é satisfatível. Como consequência, o grafo construído
é 3-colorável.

\begin{center}
\begin{tikzpicture}
  [scale=.47,auto=left,every node/.style={circle,draw=black}]
  \node[label=center:T, fill=green] (nT) at (1,10)  {\phantom{0}};
  \node[label=center:F, fill=red]   (nF) at (3,10)  {\phantom{0}};
  \node[label=center:B, fill=blue]  (nB) at (2,8)   {\phantom{0}};
  
  \node[label=center:a, fill=green] (na) at (-7.5,4)   {\phantom{0}};
  \node[label=center:$\bar{a}$, fill=red]   (nna) at (-5.5,4)  {\phantom{0}};

  \node[label=center:b, fill=red]   (nb) at (-2.5,4)  {\phantom{0}};
  \node[label=center:$\bar{b}$, fill=green] (nnb) at (0.5,4)  {\phantom{0}};

  \node[label=center:c, fill=green] (nc) at (3.5,4)   {\phantom{0}};
  \node[label=center:$\bar{c}$, fill=red]   (nnc) at (5.5,4)  {\phantom{0}};

  \node[label=center:d, fill=red]   (nd) at (8.5,4)  {\phantom{0}};
  \node[label=center:$\bar{d}$, fill=green] (nnd) at (10.5,4)  {\phantom{0}};

  \node[fill=red]   (nj11) at (-7.5,-1)  {\phantom{0}};
  \node[fill=green]   (nj12) at (-5.5,-1)  {\phantom{0}};
  \node[fill=blue]   (nj13) at (-6.5,-3)  {\phantom{0}};
  \node[fill=red]   (nj14) at (-6.5,-5)  {\phantom{0}};
  \node[fill=blue]   (nj15) at (-4.5,-5)  {\phantom{0}};
  \node[fill=green]   (nj16) at (-5.5,-7)  {\phantom{0}};

  \node[fill=green]   (nj21) at (-2.5,-1)  {\phantom{0}};
  \node[fill=blue]   (nj22) at (-0.5,-1)  {\phantom{0}};
  \node[fill=red]   (nj23) at (-1.5,-3)  {\phantom{0}};
  \node[fill=blue]   (nj24) at (-1.5,-5)  {\phantom{0}};
  \node[fill=red]   (nj25) at (0.5,-5)   {\phantom{0}};
  \node[fill=green]   (nj26) at (-0.5,-7)  {\phantom{0}};

  \node[fill=blue]   (nj31) at (3.5,-1)  {\phantom{0}};
  \node[fill=red]   (nj32) at (5.5,-1)  {\phantom{0}};
  \node[fill=green]   (nj33) at (4.5,-3)  {\phantom{0}};
  \node[fill=red]   (nj34) at (4.5,-5)  {\phantom{0}};
  \node[fill=blue]   (nj35) at (6.5,-5)  {\phantom{0}};
  \node[fill=green]   (nj36) at (5.5,-7)  {\phantom{0}};

  \node[fill=red]   (nj41) at (8.5,-1)  {\phantom{0}};
  \node[fill=blue]   (nj42) at (10.5,-1)  {\phantom{0}};
  \node[fill=green]   (nj43) at (9.5,-3)  {\phantom{0}};
  \node[fill=blue]   (nj44) at (9.5,-5)  {\phantom{0}};
  \node[fill=red]   (nj45) at (11.5,-5)  {\phantom{0}};
  \node[fill=green]   (nj46) at (10.5,-7)  {\phantom{0}};
  
  \foreach \from/\to in {nT/nF, nF/nB, nB/nT, na/nna, nB/na, nB/nna, nb/nnb, nB/nb, nB/nnb, nc/nnc, nB/nc, nB/nnc, nd/nnd, nB/nd, nB/nnd, nj11/nj12, nj11/nj13, nj12/nj13, nj13/nj14, nj14/nj15, nj14/nj16, nj15/nj16, nj21/nj22, nj21/nj23, nj22/nj23, nj23/nj24, nj24/nj25, nj24/nj26, nj25/nj26, nj31/nj32, nj31/nj33, nj32/nj33, nj33/nj34, nj34/nj35, nj34/nj36, nj35/nj36, nj41/nj42, nj41/nj43, nj42/nj43, nj43/nj44, nj44/nj45, nj44/nj46, nj45/nj46, na/nj11, nb/nj12, nb/nj21, nnc/nj22, nna/nj31, nc/nj32, nnb/nj42}
  \draw (\from) -- (\to);

  \path[draw] (na) -- (-6.5,3.4) -- (nj41);
  
  \path[draw] (nc) -- (-4,0) -- (nj15);
  \path[draw] (nnd) -- (2,0) -- (nj25);
  \path[draw] (nd) -- (7,0) -- (nj35);
  \path[draw] (nnd) -- (11.5,3.4) -- (nj45);

  \path[draw] (nj16) -- (-5.7,-8) -- (-8.5,-8) -- (-8.5,6)  -- (nB);
  \path[draw] (nj26) -- (-0.7,-8.3) -- (-8.8,-8.3) -- (-8.8,6.3)  -- (nB);
  \path[draw] (nj36) -- (5.3,-8.6) -- (-9.1,-8.6) -- (-9.1,6.6)  -- (nB);
  \path[draw] (nj46) -- (10.3,-8.9) -- (-9.4,-8.9) -- (-9.4,6.9)  -- (nB);

  \path[draw] (nj46) -- (10.7,-8.9) -- (12.5,-8.9) -- (12.5,6)  -- (nF);
  \path[draw] (nj36) -- (5.7,-9.2) -- (12.8,-9.2) -- (12.8,6.3)  -- (nF);
  \path[draw] (nj26) -- (-0.3,-9.5) -- (13.1,-9.5) -- (13.1,6.6)  -- (nF);
  \path[draw] (nj16) -- (-5.3,-9.8) -- (13.4,-9.8) -- (13.4,6.9)  -- (nF);
  
\end{tikzpicture}
\end{center}

%..............................................................................
\section{Generalizações e Restrições}

Provar que 3-coloração é NP-Completo implica em provar
que sua generalização K-coloração também é NP-Completo. Isso
pode ser dito devido ao fato de que, se pudéssemos resolver
o problema em tempo polinomial para um K arbitrário, então
poderíamos resolver em tempo polinomial para K igual a 3,
o que não é verdade como vimos na Seção anterior.

Agora que sabemos que K-coloração é uma problema NP-Completo,
podemos concluir que Coloração, na sua versão de otimização,
é NP-Dificíl.

O uso do problema 3-SAT deixou claro a importância de sabermos
que a restrição de um problema NP-Completo também faz parte
dessa classe. Essa importância se da ao fato de
que sabendo que uma restrição é NP-Completo sabemos que o
problema é intratável. Além disso, a restrição de um problema
NP-Completo pode nos levar a ter ideias para reduções à
outros problemas que até então não tínhamos certeza da sua
tratabilidade. Levando isso em consideração, gostaria de
deixar indicado algumas restrições de 3-coloração que também são
problemas NP-Completos. São esses 3-coloração, onde as
instâncias são apenas grafos planares e 3-coloração, onde
as instâncias são apenas grafos planares com grau máximo
4 \cite{Garey74}.

%..............................................................................
\section{Conclusão}

Nesse trabalho fomos apresentados ao problema de Coloração
de grafos e a prova de sua classificação na classe
NP-Difícil. A prova se deu usando a uma restrição
da sua versão de decisão e partindo de um problema
que sabíamos ser NP-Completo. Vimos também alguns exemplos
de aplicabilidade dos problemas de coloração, além
da importância de sabermos que a restrição de um certo
problema também pertence a classe NP-Completo.

%------------------------------------------------------------------------------
\clearpage
\bibliographystyle{plainnat}
\bibliography{pi}

%==============================================================================
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
