# This makefile uses latexmk
# http://www.ctan.org/pkg/latexmk
# 
# latexmk is available as a Debian package of the same name
# http://packages.debian.org/search?keywords=latexmk

#------------------------------------------------------------------------------
NAME = $(notdir $(PWD))

LATEXMK = latexmk -use-make \
	-recorder \
	-e '$$latex="latex -src-specials -interaction=nonstopmode %O %S"' \
	-e '$$pdflatex="pdflatex -src-specials -interaction=nonstopmode %O %S"' 

#------------------------------------------------------------------------------
TEXs = $(wildcard *.tex)

DVIs = $(subst .tex,.dvi,$(TEXs))

PDFs = $(subst .tex,.pdf,$(TEXs))

FIGURE_EXT = svg

FIGURE_NAMES = $(subst .$(FIGURE_EXT),,$(wildcard *.$(FIGURE_EXT)))

EPS_FIGs = $(addsuffix .eps,$(FIGURE_NAMES))

PDF_FIGs = $(addsuffix .pdf,$(FIGURE_NAMES))

#------------------------------------------------------------------------------
.PHONY : default all dvi pdf clean nofile

#------------------------------------------------------------------------------
update : nofile
	git pull --verbose --stat --progress 

all : dvi pdf

dvi : $(DVIs)

pdf : $(PDFs)

$(DVIs) : %.dvi : %.tex
	$(LATEXMK) -dvi $<

$(PDFs) : %.pdf : %.tex
	$(LATEXMK) -pdf $<

$(EPS_FIGs) : %.eps : %.svg
	inkscape --export-area-drawing \
	         --without-gui \
	         --export-latex \
	         --export-eps=$@ \
	         --file=$<

$(PDF_FIGs) : %.pdf : %.svg
	inkscape --export-area-drawing \
	         --without-gui \
	         --export-latex \
	         --export-pdf=$@ \
	         --file=$<
#------------------------------------------------------------------------------
ifneq ($(wildcard $(HOME)/lib/master.bib),)

$(NAME).bib : $(HOME)/lib/master.bib
	cp --verbose $< $@

endif

ifeq ($(USER),renato)

UFPR ?= fradim.inf.ufpr.br
MOUNT_POINT = $(UFPR):.html

upload : $(PDFs)
	rsync --verbose --archive --update --compress $^ $(MOUNT_POINT)

endif
#------------------------------------------------------------------------------
clean ::
	$(LATEXMK) -e '$$clean_full_ext = "bbl pdfsync nav snm vrb"' -C
	$(RM) -v $(EPS_FIGs) \
	         $(PDF_FIGs) \
	         $(addsuffix _tex,$(EPS_FIGs) $(PDF_FIGs)) \
	         $(addsuffix -eps-converted-to.pdf,$(FIGURE_NAMES))
